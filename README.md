# Project description

This is a testing AQA project that tests REST API with cucumber + serenity with possibility to run tests on Gitlab CI.

## The project directory structure

```Gherkin
src
+ main
   + java
     + rest                      REST API steps
+ test
   + java                        
     + step_definitions          Methods annotated with expressions
     + test_data                 Specific test data
   + resources
     + features                  Feature files
```

## Prerequisites

* Git
* Java 8
* Intellij Idea with Cucumber for Java plugin installed
* Cloned test project

## Run tests

Tests can be run in following ways:

* Via `mvn clean tests` command - to run all tests
* Via Junit runner. Run `src/test/java/TestRunner` class
* Via Cucumber Java runner (arrow button in feature files) - whole feature file or separate scenarios could be chosen
* Via Gitlab CI - `CI/CD -> Pipeline`
 
The test results will be recorded in the `target/site/serenity` directory. To generate report locally
`mvn serenity:aggregate` command should be executed.

## Write new tests

Each test has following structure:

* Feature files
* Step definitions
* Step implementations, asserts, utils etc

### Feature files example

```Gherkin
Feature: Search for the product

  Scenario Outline: Search for valid product
    When user searches for "<product>"
    Then results are displayed for "<product>"
    Examples:
      | product |
      | orange  |
      | apple   |
      | cola    |
```

### Step definitions example

```java
   @Steps
   public Rest rest;

   @When("user searches for {string}")
   public void userSearchesForProduct(String productName) {
        rest.get(SEARCH_ENDPOINT,productName);
        }

   @Then("results are displayed for {string}")
   public void resultsAreDisplayedForProduct(String productName){
        rest.verifyStatusCode(200);
        String[]expectedProductNames=Product.of(productName).getProductNames();
        rest.verifyThatEveryFieldInListInBodyContainsAnyValueFromArray("title",expectedProductNames);
        }
```

Every steps library (E.G. Rest) should be marker with `@Steps` annotation to instantiate it and to appear it Serenity test
report.

### Step library example

```java
   @Step("Get {1} using {0} endpoint")
   public Response get(String basePath, String pathParam) {
        return given()
        .baseUri(getEnvironmentVariables().getProperty("base.url"))
        .log().all()
        .basePath(basePath)
        .get(pathParam);
        }
    @Step("Verify status code is {0}")
    public void verifyStatusCode(int statusCode) {
        restAssuredThat(response -> response.statusCode(statusCode));
        }
```

Each step should be marked with `@Step` annotation to be included in Serenity report

## Refactoring done

1. Gradle build tool support is removed
2. Unnecessary files and folder are removed. Project structure is updated to be more readable and maintainable
3. Rest API logic is moved to Rest class to create reusable steps and reduce code duplication
4. Base url is moved to config file
5. Enum with product test data is created to store alternative product names for better assertions
6. Data driven approach is used for positive scenarios
7. Title verification step in positive search scenario is improved. Now it verifies that each product title has one of 
product names. It allowed to find that 'pineapple' products are displayed when searching for 'apple'. Presumably it
looks like an application defect. This scenario is currently failing because of it.
