package steps;

import io.restassured.response.Response;
import net.thucydides.core.annotations.Step;
import org.assertj.core.api.Assertions;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static net.serenitybdd.core.environment.ConfiguredEnvironment.getEnvironmentVariables;
import static net.serenitybdd.rest.SerenityRest.*;
import static org.hamcrest.Matchers.equalTo;

public class Rest {

    @Step("Get {1} using {0} endpoint")
    public Response get(String basePath, String pathParam) {
        return given()
                .baseUri(getEnvironmentVariables().getProperty("base.url"))
                .log().all()
                .basePath(basePath)
                .get(pathParam);
    }

    @Step("Verify status code is {0}")
    public void verifyStatusCode(int statusCode) {
        restAssuredThat(response -> response.statusCode(statusCode));
    }

    @Step("Verify {0} field has value {1} in body")
    public void verifyBodyValue(String path, Object expectedValue) {
        restAssuredThat(response -> response.body(path, equalTo(expectedValue)));
    }

    @Step("Verify every {0} field contains one of {1} values in body")
    public void verifyThatEveryFieldInListInBodyContainsAnyValueFromArray(String path, String[] expectedValues) {
        List<String> actualResponseBodyFieldsList = lastResponse().path(path);

        List<List<String>> words = actualResponseBodyFieldsList
                .stream()
                .map(String::toLowerCase)
                .map(s -> s.split("\\P{L}+"))
                .map(Arrays::asList)
                .collect(Collectors.toList());

        Assertions.assertThat(words)
                .isNotEmpty()
                .allMatch(field -> Arrays.stream(expectedValues).anyMatch(field::contains));
    }
}
