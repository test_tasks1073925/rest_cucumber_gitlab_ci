Feature: Search for the product

  Scenario Outline: Search for valid product
    When user searches for "<product>"
    Then results are displayed for "<product>"
    Examples:
      | product |
      | orange  |
      | apple   |
      | cola    |
      | pasta   |


  Scenario: Search for invalid product
    When user searches for "apple1"
    Then error occurs