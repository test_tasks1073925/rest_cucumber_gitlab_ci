package test_data;

public enum Product {
    ORANGE("orange", "sinas", "sinaasappel", "sinaasappelsap"),

    APPLE("apple"),
    PASTA("pasta", "pastasalade", "ovenpasta", "tagliatelle", "lasagne", "rigate", "rigatoni", "fusilli", "macaroni", "penne", "farfalle",
            "linguine", "ravioli", "gnocchi", "tortellini", "tortelloni", "cappelletti", "pappardelle", "piadina",
            "spaghetti"),
    COLA("cola");

    private final String[] productNames;

    Product(String... productNames) {
        this.productNames = productNames;
    }

    public static Product of(String productName) {
        return Product.valueOf(productName.toUpperCase());
    }

    public String[] getProductNames() {
        return this.productNames;
    }

}
