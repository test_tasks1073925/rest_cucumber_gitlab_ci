package step_definitions;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;
import steps.Rest;
import test_data.Product;

public class SearchStepDefinitions {

    private static final String SEARCH_ENDPOINT = "api/v1/search/demo/";
    @Steps
    public Rest rest;

    @When("user searches for {string}")
    public void userSearchesForProduct(String productName) {
        rest.get(SEARCH_ENDPOINT, productName);
    }

    @Then("results are displayed for {string}")
    public void resultsAreDisplayedForProduct(String productName) {
        rest.verifyStatusCode(200);
        String[] expectedProductNames = Product.of(productName).getProductNames();
        rest.verifyThatEveryFieldInListInBodyContainsAnyValueFromArray("title", expectedProductNames);
    }

    @Then("error occurs")
    public void errorOccurs() {
        rest.verifyStatusCode(404);
        rest.verifyBodyValue("detail.error", true);
    }
}
